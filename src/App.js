import React from 'react';
import './App.css';
import Board from './interfaceComponents/Board.js'
import PlayerStats from './interfaceComponents/PlayerStats'

function App() {
  return (
    <div className="App">
      <div>
        <PlayerStats />
      </div>
      <div id="board">
        <Board/>
      </div>
    </div>
      
    
  );
}

export default App;
