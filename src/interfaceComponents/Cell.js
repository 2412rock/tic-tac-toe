import React from 'react'
import '../styleComponents/Board.css'
import { Wave } from 'react-animated-text';

class Cell extends React.Component{

    constructor(args){
        super()
        this.index = args.index
        this.empty = true
        this.state = {
            character: "null",
            gameStats : args.gameStats
        }
        this.handleTap = this.handleTap.bind(this)
    }

    handleTap(){
       this.setState(lastState => {
        if(lastState.gameStats.whoseTurn() === "X"){
            lastState.character = "X"
            lastState.gameStats.makeMove(this.index)
        }else{
            lastState.character = "O"
            lastState.gameStats.makeMove(this.index)
        }
        return lastState
       })
    }

    getClassAndId(){
        if(this.index === 1 || this.index === 4 || this.index === 7){
            this.idName = 'leftCell'
        }else if(this.index % 3 === 0){
            this.idName = "rightcell"
        }
        if(this.index <= 3){
            this.className = "topCell"
            
        }else if(this.index >3 && this.index<=6){
            this.className = "topCell"
            
        }else{
            this.className = "bottomCell"
        }
    }

    render(){
        this.getClassAndId()
        return (
            <div className={this.className}  id={this.idName} onClick={this.handleTap}>
                {this.state.character === "null" ? "" : <div id="xText"><Wave text={this.state.character} effect="stretch" effectChange={2.2} /></div>}
            </div>
        );
    }

}

export default Cell