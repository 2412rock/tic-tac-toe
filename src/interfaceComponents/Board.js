import React from 'react'
import Cell from './Cell'
import GameStats from '../utils/gameStats'

function Board(){
    //render array of Cells and return it
    let cells = []
    var gameStats = new GameStats()

    for(var i = 1; i<=9;i++){
        cells[i] = <Cell index ={i} gameStats={gameStats}/>
    }
    return cells

}

export default Board