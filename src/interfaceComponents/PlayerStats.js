import React from 'react'
import PlayerStat from './PlayerStat'

class PlayerStats extends React.Component{

    constructor(args){
        super()
    }

    render(){
        return(
            <div>
                <PlayerStat className = "PlayerStats" player = "X"/>
                <PlayerStat className = "PlayerStats" player= "O"/>
            </div>
        )
    }
}

export default PlayerStats