class GameStats{
    constructor(){
        this.xMoves = true
        this.cells = []
    }
    
    makeMove(cellIndex){
            this.cells[cellIndex] = this.xMoves ? "X" : "O"
            this.xMoves = !this.xMoves
            return true
    }

    whoseTurn(){
        if(this.xMoves){
            console.log("X moves")
        }else{
            console.log("O moves")
        }
        return this.xMoves ? "X" : "O"
    }



}


export default GameStats